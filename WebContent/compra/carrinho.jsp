<%@page import="model.Usuario"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="model.ItemCompra"%>
<%@page import="model.Compra"%>
<%@page import="model.Produto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page = "../includes/dependences.html" />
<jsp:include page = "../includes/header.jsp" />

<%
	Produto prdCompra = new Produto();
Compra carrinho = (Compra)session.getAttribute("carrinho") == null ? new Compra() : (Compra)session.getAttribute("carrinho");

boolean novaCompra = false;

Usuario usuarioLogado = (Usuario)session.getAttribute("usuarioLogado");


//session.removeAttribute("carrinho");
if(request.getMethod().equals("GET")){
	if(request.getParameter("idPrd") != null){
		novaCompra = true;
		prdCompra = new Produto();
		prdCompra.setId(Integer.parseInt(request.getParameter("idPrd")));
		prdCompra = prdCompra.find();
		if(session.getAttribute("carrinho") != null){
	carrinho = (Compra)session.getAttribute("carrinho");
	//session.removeAttribute("carrinho");
	for(ItemCompra itemCompraCarrinho : carrinho.getItensCompra()){
		if(itemCompraCarrinho.getProduto().getId() == prdCompra.getId()){
			novaCompra = false;
		}
	}
		}
	}else if(request.getParameter("del_prod") != null){
		carrinho = (Compra)session.getAttribute("carrinho");
		session.removeAttribute("carrinho");
		
		carrinho.getItensCompra().remove(Integer.parseInt(request.getParameter("del_prod")));
		
		session.setAttribute("carrinho", carrinho);
		
		response.sendRedirect("carrinho.jsp");
	}
	
	/*ItemCompra itemCarrinho = new ItemCompra();
	itemCarrinho.setProduto(newProduto);
	itemCarrinho.setPreco(newProduto.getPreco());
	
	carrinho.addItemCompra(itemCarrinho);*/
	
	//session.setAttribute("carrinho", carrinho);
	
}





if(request.getMethod().equals("POST")){
	if(request.getParameter("carrinho") != null){
		session.removeAttribute("carrinho");
		
		Compra carrinhoCompra = new Compra();
		
		carrinhoCompra.popularCarrinho(request);
		
		session.setAttribute("carrinho", carrinhoCompra);
		
		response.sendRedirect("../index.jsp");
	}else if(request.getParameter("finalizarCompra") != null){
		Compra carrinhoCompra = new Compra();
		
		carrinhoCompra.popularCarrinho(request);
		
		carrinhoCompra.setUsuarioComprador(usuarioLogado);
		
		carrinhoCompra.save();
		
		session.removeAttribute("carrinho");
		
		response.sendRedirect("../index.jsp");
	}
}
%>
<style>
    .title{
        border-radius:20px 20px 0 0;
        padding:10px;
    }
    .MyCarret{
        border-radius:20px 20px 0 0;
        border-left:2px solid #3779B3;
        border-right:2px solid #3779B3;
        border-bottom:2px solid #3779B3;
        padding:20px;
    }
    
    </style>
    <div id="modalFinalizaCompra" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Total compra</h4>
	      </div>
	      <div class="modal-body">
	        <h3>O total da compra é: <b>R$ <font id="totalCompra"></font></b></h3>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        <button type="button" id="confirmarCompra" class="btn btn-success" data-dismiss="modal">Confirmar</button>
	      </div>
	    </div>
	
	  </div>
	</div>
    <div class="container">
        <form method="POST" role="form" id="form_carrinho">

            <%
            	if(novaCompra){
            %>
            <div class="panel panel-primary">
                <div class='panel-heading text-center'><h1>Novo produto <span class='glyphicon glyphicon-shopping-cart'></span></h1></div>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-sm-5">
                            <div class="form-group">
                            <label for="nome_prd">Produto:</label>
                                <input type="hidden" name="id_produto" value="<%=prdCompra.getId()%>">
                                <input type="hidden" name="qtd_estoque" value=<%=prdCompra.getQtdEstoque()%>>
                                <input type="text" readonly='true' class="form-control" name="nome_prd" id="nome_prd" value="<%=prdCompra.getNome()%>">
                            </div>
                        </div>
                        <div class="preco_tot">
	                        <div class="col-sm-4">
	                            <div class="form-group">
	                                <label for="qtdProduto">Preço:</label>
	                                <input type="text" readonly="true" class="form-control" value="<%=prdCompra.getPreco()%>" name="preco" id="preco">
	                            </div>
	                        </div>
	                        <div class="col-sm-3">
	                            <div class="form-group">
	                                <label for="qtdProduto">Quantidade:</label>
	                                <input type="number" name="qtd_produto" class="form-control qtd_prd" min="1"  max="<%=prdCompra.getQtdEstoque()%>" required value='1' step="1" required="required">
	                            </div>
	                        </div>
                        </div>

                    </div>
                </div>
            </div>
                <div class="text-right">
                    <button type="submit" name="carrinho" class="btn btn-primary">Adicionar ao carrinho e continuar comprando</button>
                </div>
                <br>
            <%
            	}
            %>
                
                <%
                                	if(carrinho.getItensCompra() != null && !carrinho.getItensCompra().isEmpty()){
                                                	out.print("<div class='panel panel-primary'>");
                                                	out.print("<div class='panel-heading text-center'><h1>Meu carrinho <span class='glyphicon glyphicon-shopping-cart'></span></h1></div>");
                                                	int ind = 0;
                                                    for(ItemCompra itemCompra : carrinho.getItensCompra()){
                                %>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                <label for="nome_prd">Produto:</label>
                                    <input type="hidden" name="id_produto" value="<%=itemCompra.getProduto().getId()%>">
                                    <input type="hidden" name="qtd_estoque" value="<%=itemCompra.getProduto().getQtdEstoque()%>">
                                    <input type="text" readonly="true" class="form-control" name="nome_prd" id="nome_prd" value="<%=itemCompra.getProduto().getNome()%>">
                                </div>
                            </div>
                            <div class="preco_tot">
	                            <div class="col-sm-3">
	                                <div class="form-group">
	                                    <label for="qtdProduto">Preço:</label>
	                                    <input type="text" readonly="true" class="form-control" value="<%=itemCompra.getProduto().getPreco()%>" name="preco" id="preco">
	                                </div>
	                            </div>
	                            <div class="col-sm-3">
	                                <div class="form-group">
	                                    <label for="qtdProduto">Quantidade:</label>
	                                    <input type="number" name="qtd_produto" class="form-control qtd_prd" min="1"  max="<%=itemCompra.getProduto().getQtdEstoque()%>" required value='<%=itemCompra.getQtd()%>' step="1" required="required">
	                                </div>
	                            </div>
	                        </div>
                            <div class="col-sm-1">
                                <label for="nome_prd">Excluir</label>
                                <a href="carrinho.jsp?del_prod=<%=ind%>" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
                            </div>
                        </div>
                    </div>

                <%
                	ind++;
                                    }
                                    out.print("</div>");
                                }
                            //print_r($nova_compra);
                            if(novaCompra || !carrinho.getItensCompra().isEmpty()){
                %>
            
            
            <div class="text-right">
                
                <%
                if(usuarioLogado != null){
                %>
                <button type="button" id="finalizaCompra" class="btn btn-success">Finalizar compra</button>
                <button type="submit" name="finalizarCompra" id="confirmaCompraHidden" style="display:none;"></button>
                <%
                }else{
                %>
                <button type="button" data-toggle="modal" data-target="#loginModal" class="btn btn-success">Fazer login</button>
                <%
                }
                %>
                
            </div>
            <%
            }else{
            	out.print("<div class='text-center alert alert-success'><h3><strong>Carrinho vazio <span class='glyphicon glyphicon-shopping-cart'></span></strong></h3></div>");
            }
            %>
        </form>
    </div>
    <script>
	    $(document).ready(function(){
	    	var totalCompra = 0;
	    	calculaTotalCompra();
	    	$(".qtd_prd").change(function(){
	    		calculaTotalCompra();
	    	});
	    	
	    	function calculaTotalCompra(){
	    		totalCompra = 0;
	    		$(".qtd_prd").each(function(index,qtd){
		    		totalCompra += $(qtd).parents('.preco_tot').children().first().find(".form-group").children().last().val() * $(qtd).val();
		    	});
				console.log(totalCompra);
	    	}
	    	
	    	$("#finalizaCompra").click(()=>{
	    		$("#totalCompra").html(totalCompra);
	    		$("#modalFinalizaCompra").modal();
	    	});
	    	
	    	$("#confirmarCompra").click(()=>{
	    		$("#confirmaCompraHidden").click();
	    	});
	    	
	    });
    </script>
    </body>
</html>