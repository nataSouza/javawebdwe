<%@page import="model.ItemCompra"%>
<%@page import="model.Compra"%>
<%@page import="model.Usuario"%>
<%@page import="utils.Constants"%>
<%@page import="utils.Util"%>
<%@page import="java.util.List"%>
<%@page import="model.Produto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page = "../includes/dependences.html" />
<jsp:include page = "../includes/header.jsp" />
<%

Usuario usuarioLogado = (Usuario)session.getAttribute("usuarioLogado");

List<Compra> minhasCompras = null;

if(usuarioLogado == null){
	response.sendRedirect("../index.jsp");
}else{
	minhasCompras = usuarioLogado.listMinhasCompras();
}

%>
	<!-- Modal de itens da compra -->
	<div id="modalItensCompra" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Itens da compra</h4>
	      </div>
	      <div class="modal-body">
	        <table class="table table-condensed" id="tableItensCompra" width="100%">
	        	<thead>
	        		<tr>
	        			<td>Foto</td>
	        			<td>Nome</td>
	        			<td>Preço</td>
	        			<td>Quantidade</td>
	        		</tr>
	        	</thead>
	        	<tbody id="itensCompraTbody">
	        	</tbody>
	        </table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	<!-- Fim Modal -->
	
	<div class="container">
		<%
		if(minhasCompras != null){
		%>
			<div class="tableParent">
		        <table class="table tableDataTable table-striped table-condensed dt-responsive nowrap">
		            <thead>
		                <tr>
		                    <th>Data da compra</th>
		                    <th>Total</th>
		                    <th>Detalhes</th>
		                </tr>
		            </thead>
		            <tbody>
					<%
						for(Compra compra : minhasCompras){
					%>
				            <tr>
				                <td align="center"><%=compra.getDataCompra() %></td>
								<td align="center">R$ <%=compra.getTotalCompra() %></td>
								<td align="center">
									<button class="btn btn-xs btn-primary verCompra" data-id-compra="<%=compra.getId()%>"><span class="glyphicon glyphicon-eye-open"></span></button>
								</td>
				            </tr>
			
					<%		
						}
					%>
			    	</tbody>
	            </table>
	        </div>
		<%
		}else{
		%>
			<div class="text-center alert alert-warning">
             	<h3><strong>Nenhum compra efetuada <span class="glyphicon glyphicon-shopping-cart"></span></strong></h3>
            </div>
		<%
		}
		%>
        
    </div>
<jsp:include page = "../includes/footer.html" />
<script type="text/javascript" src="../assets/js/script.js"></script>
<script>
$(document).ready(function(){
	$('#tableItensCompra').DataTable({
        "oLanguage": {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "Não foram encontrados resultados",
            "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
            "sInfoFiltered": "",
            "sInfoPostFix":  "",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Seguinte",
                "sLast":     "Último"
            }
        }
    });
	$(".verCompra").click(function(){
		var idCompra = $(this).data("idCompra");
		
		$.get("../ajax/itensCompra.jsp?IdCompra="+idCompra,(itensCompra)=>{
			var tbody = "";
			$(itensCompra).each((index, itemCompra)=>{
				tbody += "<tr>";
				tbody += "<td align='center'><img style='width:30%' class='img-responsive' src='"+itemCompra.dirImage+"'></td>";
				tbody += "<td align='center'>"+itemCompra.nomeProduto+"</td>";
				tbody += "<td align='center'>R$ "+itemCompra.preco+"</td>";
				tbody += "<td align='center'>"+itemCompra.qtd+"</td>";
				tbody += "</tr>";
			});
			
			
			$('#tableItensCompra').DataTable().clear();
			$('#tableItensCompra').DataTable().destroy();
			$('#itensCompraTbody').html(tbody);
			$('#tableItensCompra').DataTable({
		        "oLanguage": {
		            "sProcessing":   "Processando...",
		            "sLengthMenu":   "Mostrar _MENU_ registros",
		            "sZeroRecords":  "Não foram encontrados resultados",
		            "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
		            "sInfoFiltered": "",
		            "sInfoPostFix":  "",
		            "sSearch":       "Buscar:",
		            "sUrl":          "",
		            "oPaginate": {
		                "sFirst":    "Primeiro",
		                "sPrevious": "Anterior",
		                "sNext":     "Seguinte",
		                "sLast":     "Último"
		            }
		        }
		    });
			
			
			
			$('#modalItensCompra').modal();
		});
	});
});

</script>
</body>
</html>