<%@page import="model.Compra"%>
<%@page import="utils.Constants"%>
<%@page import="model.Funcionario"%>
<%@page import="model.Usuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
    String msg = "";
    if(request.getMethod().equals("POST") && request.getParameter("btnRealizaLogin") != null){
    	Usuario usuario = new Usuario();
    	usuario.setEmail(request.getParameter("email"));
    	usuario.setSenha(request.getParameter("senha"));
    	usuario.login();
    	
    	if(usuario.getId() > 0){
    		session.setAttribute("usuarioLogado", usuario);
    	}else{
    		msg = "Login ou senha invalidos";
    	}
    }

    if(request.getMethod().equals("GET") && request.getParameter("action") != null && request.getParameter("action").equals("logout")){
    	session.removeAttribute("usuarioLogado");
    	
    	response.sendRedirect("../index.jsp");
    }

    Usuario usuarioLogado = (Usuario)session.getAttribute("usuarioLogado");
    Compra carrinho = (Compra)session.getAttribute("carrinho");

    int qtdCarrinho = carrinho == null ? 0 : carrinho.getItensCompra().size();
    String badgeCarrinho = carrinho == null || carrinho.getItensCompra().size() == 0? "success" : "primary";

    //out.print(usuarioLogado.getTipoUsuario());
    %>

<style>
	.navbar{
		margin-bottom:0px;
	}
	.container{
		margin-top:20px;
	}
</style>
<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Logar</h4>
        </div>
        <form method="POST" role="form">
            <div class="modal-body">
                <div class="form-group">
                    <label for="txEmail">Email</label>
                    <input type="email" class="form-control" name="email" id="txEmail" placeholder="exemple@exemple.com">
                </div>

                <div class="form-group">
                    <label for="txSenha">Senha</label>
                    <input type="password" class="form-control" name="senha" id="txSenha" placeholder="**********">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="btnRealizaLogin" class="btn btn-success">Logar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </form>
        </div>
    </div>
</div>
<nav class="navbar navbar-default" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container">
	     <div class="navbar-header">
	         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	             <span class="sr-only">Toggle navigation</span>
	             <span class="icon-bar"></span>
	             <span class="icon-bar"></span>
	             <span class="icon-bar"></span>
	         </button>
	         <a class="navbar-brand" href="../index.jsp"><span class="glyphicon glyphicon-shopping-cart"></span></a>
	     </div>
 
     <!-- Collect the nav links, forms, and other content for toggling -->
     
         <div class="collapse navbar-collapse navbar-ex1-collapse">
             <ul class="nav navbar-nav">
                 <li class="active"><a href="../index.jsp">Home</a></li>
                 <!--<li><a href="#">Produtos</a></li>-->
             </ul>
             <ul class="nav navbar-nav navbar-right">
                 <li class="dropdown">
                 	<%
                 		if(usuarioLogado != null){
                 	%>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Bem-vindo ${usuarioLogado.getNome()} </b><b class="caret"></b></a>
                   	<ul class="dropdown-menu">
                    	
                    
                 	<%
                                     		if(usuarioLogado.isAdm()){
                                     	                 			out.print("<li><a href='../produto'>Todos os produtos</a></li>");
                                     	                 			out.print("<li><a href='../usuario'>Todos os usuarios</a></li>");
                                     	                 		}else if(usuarioLogado.isFuncionario()){
                                     	                 			out.print("<li><a href='../produto'>Todos os produtos</a></li>");
                                     	                 		}
                                     	%>
                 	
                 	
                 		<li><a href="../compra/minhas_compras.jsp">Minhas compras</span></a></li>
                 		<li><a href="../usuario/form.jsp?id=<%=usuarioLogado.getId()%>">Meu perfil</span></a></li>
                 		<li><a href="../view/index.jsp?action=logout">Logout</a></li>
                 	</ul>
                 	<%
                 		}else{
                 	%>
                    <div class="btn-group">
	                    <button href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-success navbar-btn">Fazer login</button>
	                    <a href="../usuario/form.jsp" class="btn btn-success navbar-btn">Cadastrar-se</a>
                    </div>
                    <%
                    	}
                    %>
                 </li>
                 <a href="../compra/carrinho.jsp" class="btn btn-<%=badgeCarrinho%> navbar-btn "><span class="glyphicon glyphicon-shopping-cart"></span><span class='badge'><%=carrinho == null ? 0 : carrinho.getItensCompra().size()%></span></a>
             </ul>
         </div><!-- /.navbar-collapse -->
     </div>
 </nav>
 
 <%
 if(!msg.equals("")){
 %>
 <div class="alert alert-danger">
  <strong>Erro !</strong> <%=msg %>
 </div>
 <%
 }
 %>
