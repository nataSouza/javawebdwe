$(".delete").click(function(){
	var id = $(this).attr('rel');
	
	utils.confirm("Deletar","Deseja mesmo deletar?",function(){
		location.href = "index.jsp?id="+id;
	});
});

var utils = {
	confirm: function(title, msg, callback){
		
		var modal = '<div id="modalConfirm" class="modal fade" role="dialog">'+
			  		'<div class="modal-dialog">'+
					    '<div class="modal-content">'+
					      '<div class="modal-header">'+
					        '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
					        '<h4 class="modal-title">'+title+'</h4>'+
					      '</div>'+
					      '<div class="modal-body">'+
					        '<p>'+msg+'</p>'+
					      '</div>'+
					      '<div class="modal-footer">'+
					        '<button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>'+
					        '<button type="button" id="btnConfirm" class="btn btn-primary">Sim</button>'+
					      '</div>'+
					    '</div>'+
					  '</div>'+
					'</div>';
				
		$("body").append(modal);
		
		$("#modalConfirm").modal();
		
		$('#btnConfirm').click(function(){
			$("#modalConfirm").modal('hide');
			callback();
		});
		
		$("#modalConfirm").on("hide.bs.modal",function(){
			$(this).remove();
		});
		
	}
}