<%@page import="model.Usuario"%>
<%@page import="utils.Util"%>
<%@page import="java.util.List"%>
<%@page import="model.Produto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page = "../includes/dependences.html" />
<jsp:include page = "../includes/header.jsp" />
<%
if(request.getParameter("id") != null && request.getMethod().equals("GET")){
	Produto produto = new Produto();
	produto.setId(Integer.parseInt(request.getParameter("id")));	
	produto.delete();
    response.sendRedirect("index.jsp");
}
%>
	<style>
.thumbnail{
    box-shadow: 0px 5px 18px #888888;
}
.price{
    width:100%;
    font-size:1.4em;
}
.image-itens{
    min-height:30vh;
    max-height:30vh;
    vertical-align:middle;
    position: relative;
}
.image-item{
    max-height: 100%;  
    max-width: 100%; 
    width: auto;
    height: auto;
    position: absolute;  
    top: 0;  
    bottom: 0;  
    left: 0;  
    right: 0;  
    margin: auto;
}
</style>
    <?=$mensagem?>
    <div class="container">
        <div class="row">
        
	        <%	        
	        List<Produto> produtos = new Produto().grid();
	        if(produtos.size() > 0){
	            for(Produto produto : produtos){
	       %>

                
                <div class="col-sm-4">     
                    <div class="thumbnail">
                        <div>
                        <div class="image-itens">
                            <img src="<%=produto.getDirImage() != null ? produto.getDirImage() : "../assets/images/default-image.png" %>" class="image-item img-responsive" >
                        </div>
                        <div class="caption text-center">
                            <h3><%=produto.getNome() %></h3>
                            <h4 class="price badge bg-success">R$ <%=produto.getPreco() %></h4>
                            <h4><%=produto.getLabelProduto() %></h4>
                            <p><a href="../compra/carrinho.jsp?idPrd=<%=produto.getId() %>" class="btn btn-success">Comprar <span class="glyphicon glyphicon-shopping-cart"></span></a></p>
                        </div>
                        </div>
                    </div>       
                </div>
	        <%
	            }
	        }else{
	        %>
	            <div class="text-center alert alert-warning">
	             	<h3><strong>Nenhum produto disponível <span class="glyphicon glyphicon-shopping-cart"></span></strong></h3>
	            </div>';
	        <%
	        }
	        %>
        </div>
    </div>
    </body>
</html>