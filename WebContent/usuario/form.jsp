
<%@page import="model.Funcionario"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Usuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page = "../includes/dependences.html" />
<jsp:include page = "../includes/header.jsp" />

<%
Usuario usuario = new Usuario();

Usuario usuarioLogado = (Usuario)session.getAttribute("usuarioLogado");

if(request.getParameter("login") != null && request.getMethod().equals("POST")){
	
	usuario.setId(Integer.parseInt(request.getParameter("id")));
	usuario.setNome(request.getParameter("nome"));
	usuario.setLogin(request.getParameter("login"));
	usuario.setSenha(request.getParameter("senha"));
	usuario.setEmail(request.getParameter("email"));
	usuario.setTipoUsuario(request.getParameter("tipoUsuario") != null ? request.getParameter("tipoUsuario") : "U");
	usuario.setStatus(request.getParameter("status") != null ? "A" : "I");

	usuario.save();
	
    response.sendRedirect("index.jsp");
}

if(request.getParameter("id") != null && request.getMethod().equals("GET")){
	usuario.setId(Integer.parseInt(request.getParameter("id")));
	usuario = usuario.find();
}
%>
<div class="container">	
	<form name="frmUser" id="frmUser" method="post">
		<input type="hidden" name="id" value="<%=usuario.getId()%>">
		<div class="form-group">
		    <label for="nome">Nome:</label>
		    <input type="text" name="nome" value="<%=usuario.getNome()%>" class="form-control" id="nome">
		</div>
		<div class="form-group">
		    <label for="email">Email:</label>
		    <input type="text" name="email" value="<%=usuario.getEmail()%>" class="form-control" id="email">
		</div>
		<div class="form-group">
		    <label for="login">Login:</label>
		    <input type="text" name="login" value="<%=usuario.getLogin()%>" class="form-control" id="login">
		</div>
		
		<div class="form-group">
		    <label for="senha">Senha:</label>
		    <input type="password" name="senha" value="<%=usuario.getSenha()%>" class="form-control" id="senha">
		</div>
		
		<%
		if(usuarioLogado != null && usuarioLogado.isAdm()){
		%>
		
		<div class="form-group">
			<select class="form-control" name="tipoUsuario">
				<option <%=usuario.isCliente() ? "selected" : "" %> value="U">Usuário</option>
				<option <%=usuario.isFuncionario() ? "selected" : "" %> value="F">Funcionário</option>
				<option <%=usuario.isAdm() ? "selected" : "" %> value="A">Administrador</option>
			</select>
		</div>
		
		<%
		}
		%>
		
		<div class="form-group">
		    <label for="status">Ativo</label>
		    <input type="checkbox" name="status" <%=usuario.getStatus().equals("A") ? "checked" : ""%> value="A" class="form-control" id="status">
		</div>
		
		<div class="text-right">
			<button type="submit" class="btn btn-primary">Salvar</button>
		</div>
		
	</form>
</div>
</body>
</html>