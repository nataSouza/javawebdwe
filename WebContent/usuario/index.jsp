
<%@page import="utils.Util"%>
<%@page import="com.sun.java.swing.plaf.windows.resources.windows"%>
<%@page import="model.Usuario"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page = "../includes/dependences.html" /> 
<jsp:include page = "../includes/header.jsp" />

<%

Usuario usuarioLogado = (Usuario)session.getAttribute("usuarioLogado");

Util.accessControl(usuarioLogado, "A", "../index.jsp", response);

if(request.getParameter("id") != null && request.getMethod().equals("GET")){
	Usuario usuario = new Usuario();
	usuario.setId(Integer.parseInt(request.getParameter("id")));		
	usuario.delete();
    response.sendRedirect("index.jsp");
}


Usuario usuario = new Usuario();

List<Usuario> usuarios = usuario.list();
%>
<div class="container">	
	<div class="text-right">
		<a href="form.jsp" class="btn btn-primary">
			Adicionar novo
		</a>
	</div>
	<br/>
	<table class="table tableDataTable table-striped">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Nome</th>
		    <th>Email</th>
		    <th>Login</th>
		    <th>Ativo</th>
		    <th>Ações</th>
		  </tr>
		</thead>
		<tbody>
		<%
			for(Usuario user: usuarios){
		%>
		<tr>
			<td><%=user.getId() %></td>
			<td><%=user.getNome() %></td>
			<td><%=user.getEmail() %></td>
			<td><%=user.getLogin() %></td>
			<td><%=user.getStatusFormatado() %></td>
			<td>
				<a class="btn btn-info btn-sm" href="form.jsp?id=<%=user.getId()%>"><span class="glyphicon glyphicon-pencil"></span></a>
				<button class="btn btn-danger btn-sm delete" rel="<%=user.getId()%>"><span class="glyphicon glyphicon-trash"></span></button>
			</td>
		</tr>
		<%
		}
		%>
		</tbody>
	</table>
</div>

<jsp:include page = "../includes/footer.html" />

</body>
<script type="text/javascript" src="../assets/js/script.js?version=1.0"></script>
</html>