<%@page import="utils.Constants"%>
<%@page import="model.Usuario"%>
<%@page import="utils.Util"%>
<%@page import="org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.tomcat.util.http.fileupload.FileUploadException"%>
<%@page import="java.io.InputStream"%>
<%@page import="org.apache.tomcat.util.http.fileupload.FileItemStream"%>
<%@page import="org.apache.tomcat.util.http.fileupload.FileItemIterator"%>
<%@page import="org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="model.Produto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page = "../includes/dependences.html" />
<jsp:include page = "../includes/header.jsp" />
<%
Usuario usuarioLogado = (Usuario)session.getAttribute("usuarioLogado");
Util.accessControl(usuarioLogado, Constants.FUNCIONARIO, "../index.jsp", response);
Produto produto = new Produto();
if(request.getMethod().equals("POST")){
	if(new Produto().save(request)){
		response.sendRedirect("index.jsp");
	}else{
		out.print("Houve algum problema");
	}
}
if(request.getMethod().equals("GET") && request.getParameter("id") != null){
	produto.setId(Integer.parseInt(request.getParameter("id")));
	produto = produto.find();
}
%>

	<div class="container">
        <form method="post" role="form" enctype="multipart/form-data">
            <legend>Cadastro de Produto</legend>
            <input type="hidden" name="id" value="<%=produto.getId()%>">
            <input type="hidden" name="last_dir_image" value="<%=produto.getDirImage()%>">
            <div class="form-group">
                <label for="nomeprod">Nome do produto</label>
                <input type="text" class="form-control" name="nome" id="nomeprod" placeholder="Leite condensado ..."  value="<%=produto.getNome()%>">
            </div>
            <div class="form-group">
                <label for="codBarras">Codigo de Barras</label>
                <input type="text" class="form-control" name="codigo_barras" id="codBarras"   value="<%=produto.getCodigoBarras()%>">
            </div>
            <div class="form-group">
                <label for="preco">Preço</label>
                <input type="text" class="form-control" name="preco" id="preco" placeholder="10.60" required  value="<%=produto.getPreco()%>">
            </div>
            <div class="form-group">
                <label for="qtd_estoque">Estoque</label>
                <input type="number" class="form-control" name="qtd_estoque" id="qtd_estoque" min="1" placeholder="0" required value="<%=produto.getQtdEstoque()%>">
            </div>

            <div class="form-group">
                <label for="exampleFormControlFile1">Imagem</label>
                <input name="imagem_produto" type="file" class="form-control-file" id="exampleFormControlFile1">
            </div>
            <div class="text-right">
                <button type="submit" name="cadProd" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
<jsp:include page = "../includes/footer.html" />
   
<script>

$(document).ready(function(){
	$('#preco').mask('99.99');	
})


</script>
</body>
</html>