<%@page import="model.Usuario"%>
<%@page import="utils.Constants"%>
<%@page import="utils.Util"%>
<%@page import="java.util.List"%>
<%@page import="model.Produto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page = "../includes/dependences.html" />
<jsp:include page = "../includes/header.jsp" />
<%

Usuario usuarioLogado = (Usuario)session.getAttribute("usuarioLogado");
Util.accessControl(usuarioLogado, Constants.FUNCIONARIO, "../index.jsp", response);

if(request.getParameter("id") != null && request.getMethod().equals("GET")){
	Produto produto = new Produto();
	produto.setId(Integer.parseInt(request.getParameter("id")));	
	produto.delete();
    response.sendRedirect("index.jsp");
}
%>
	<div class="container">
        <div class="text-right">
            <a href="form.jsp" class="btn btn-success">Adicionar novo</a>
        </div>
        <div class="tableParent">
            <table class="table tableDataTable table-striped table-condensed dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Em estoque</th>
                        <th>Preço</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>

                    <%
                    Produto produto = new Produto();
                    
                    List<Produto> produtos = produto.list();
                    for(Produto prd : produtos){
                    %>
                    <tr>
                        <td><%=prd.getNome() %></td>
						<td><%=prd.getQtdEstoque() %></td>
						<td><%=prd.getPreco() %></td>
                        <td>
                            <a href="form.jsp?id=<%=prd.getId() %>" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>
                            <button rel="<%=prd.getId() %>" class="btn btn-danger delete"><span class="glyphicon glyphicon-remove"></span></button>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
    </div>
<jsp:include page = "../includes/footer.html" />
<script type="text/javascript" src="../assets/js/script.js"></script>
</body>
</html>