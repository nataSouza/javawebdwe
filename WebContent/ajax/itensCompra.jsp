<%@page import="org.json.JSONArray"%>
<%@page import="model.ItemCompra"%>
<%@page import="java.util.List"%>
<%@page import="model.Compra"%>
<%@ page language="java" contentType="json; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
if(request.getParameter("IdCompra") == null){
	out.print("nulla");
}else{
	Compra compra = new Compra();
	compra.setId(Integer.parseInt(request.getParameter("IdCompra")));
	compra = compra.find();
	
	
	JSONArray jsonArrayitensCompra = new ItemCompra().listByCompra(compra);
	
	out.print(jsonArrayitensCompra.toString());
	
}
%>