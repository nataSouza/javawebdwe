package utils;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Usuario;

public class Util {
	
	public static void accessControl(Usuario usuarioLogado,String tipoUsuarioNecessario,String redirect,HttpServletResponse response) {
		System.out.println("Teste");
		
		if(usuarioLogado == null) {
			try {
				response.sendRedirect(redirect);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(!usuarioLogado.getTipoUsuario().equals(tipoUsuarioNecessario) && !usuarioLogado.getTipoUsuario().equals(Constants.ADM)) {
			try {
				response.sendRedirect(redirect);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
