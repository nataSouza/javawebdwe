package model;

import java.io.Serializable;
import javax.persistence.*;

import database.DAO;
import utils.Constants;

import java.util.List;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@Table(name="usuario")

@NamedQueries({
	@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u"),
	@NamedQuery(name="Usuario.login", query="SELECT u FROM Usuario u WHERE u.email = :email AND u.senha = :senha AND u.status = :status"),
	@NamedQuery(name="Usuario.minhasCompras", query="SELECT c FROM Compra c WHERE c.comprador = :comprador")
})

public class Usuario extends DAO<Usuario> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	private int id = 0;

	private String email = "";

	@Column(name="Login")
	private String login = "";

	private String nome = "";

	private String senha = "";

	private String status = "A";
	
	private String tipoUsuario = "U";
	
	//bi-directional many-to-one association to Compra
	@OneToMany(mappedBy="comprador",cascade = CascadeType.ALL)
	private List<Compra> carrinho;

	//bi-directional many-to-one association to Compra
	@OneToMany(mappedBy="usuario2",cascade = CascadeType.ALL)
	private List<Compra> compras2;


	public Usuario() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getStatus() {
		return this.status;
	}
	
	public String getStatusFormatado() {
		return this.getStatus().equals("A") ? "Ativo" : "Inativo";
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Compra> getCarrinhos() {
		return this.carrinho;
	}
	
	public Compra getCarrinho() {
		return this.carrinho.get(0);
	}

	public void setCarrinho(List<Compra> carrinho) {
		this.carrinho = carrinho;
	}

	public Compra addCarrinho(Compra carrinho) {
		getCarrinhos().add(carrinho);
		carrinho.setUsuarioComprador(this);

		return carrinho;
	}
	
	public ItemCompra addItemCarrinho(ItemCompra itemCompra) {
		this.getCarrinho().getItensCompra().add(itemCompra);		
		return itemCompra;
	}

	public Compra removeCarrinho(Compra carrinho) {
		getCarrinhos().remove(carrinho);
		carrinho.setUsuarioComprador(null);

		return carrinho;
	}

	public List<Compra> getCompras2() {
		return this.compras2;
	}

	public void setCompras2(List<Compra> compras2) {
		this.compras2 = compras2;
	}

	public Compra addCompras2(Compra compras2) {
		getCompras2().add(compras2);
		compras2.setUsuario2(this);

		return compras2;
	}

	public Compra removeCompras2(Compra compras2) {
		getCompras2().remove(compras2);
		compras2.setUsuario2(null);

		return compras2;
	}
	
	public void login() {
		Usuario usuario = null;
		try {
			usuario = super.em.createNamedQuery("Usuario.login",Usuario.class)
					.setParameter("email", this.getEmail())
					.setParameter("senha", this.getSenha())
					.setParameter("status", "A")
					.getSingleResult();
		}catch(NoResultException e) {
			e.getStackTrace();
		}
		
		
		if(usuario != null) {
			this.setId(usuario.getId());
			this.setNome(usuario.getNome());
			this.setLogin(usuario.getLogin());
			this.setTipoUsuario(usuario.getTipoUsuario());
			//this.setFuncionario(usuario.getFuncionario());
		}
	}
	
	public String getTipoUsuario() {
		return this.tipoUsuario;
	}
	
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	public boolean isFuncionario() {
		return this.getTipoUsuario().equals("F");
	}
	
	public boolean isAdm() {
		return this.getTipoUsuario().equals("A");
	}
	
	public boolean isCliente() {
		return this.getTipoUsuario().equals("U");
	}
	
	public List<Compra> listMinhasCompras(){
		try {
			return super.em.createNamedQuery("Usuario.minhasCompras",Compra.class)
					.setParameter("comprador", this)
					.getResultList();
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
			// TODO: handle exception
		}
	}
	
	/*public List<Compra> minhasCompras(){
		return this.em.createQuery("SELECT c FROM Compra c WHERE id_comprador=:idComprador GROUP BY ",Compra.class)
				.setParameter("idComprador", this.getId())
				.getResultList();
	}*/

}