package model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;

import database.DAO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the compra database table.
 * 
 */
@Entity
@Table(name="compra")
@NamedQuery(name="Compra.findAll", query="SELECT c FROM Compra c")
public class Compra extends DAO<Compra> implements Serializable {
	private static final long serialVersionUID = 1L;

	@GeneratedValue
	@Id
	private int id;

	@Column(name="data_compra")
	private Timestamp dataCompra;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_comprador")
	private Usuario comprador;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_funcionario")
	private Usuario usuario2;

	//bi-directional many-to-one association to ItemCompra
	@OneToMany(mappedBy="compra",cascade=CascadeType.PERSIST)
	private List<ItemCompra> itensCompra;

	public Compra() {
		this.itensCompra = new ArrayList<ItemCompra>();
	}
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getDataCompra() {
		return this.dataCompra;
	}

	public void setDataCompra(Timestamp dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Usuario getUsuarioComprador() {
		return this.comprador;
	}

	public void setUsuarioComprador(Usuario comprador) {
		this.comprador = comprador;
	}

	public Usuario getUsuario2() {
		return this.usuario2;
	}

	public void setUsuario2(Usuario usuario2) {
		this.usuario2 = usuario2;
	}

	public List<ItemCompra> getItensCompra() {
		return this.itensCompra;
	}

	public void setItemCompras(List<ItemCompra> itemCompras) {
		this.itensCompra = itemCompras;
	}

	public ItemCompra addItemCompra(ItemCompra itemCompra) {
		getItensCompra().add(itemCompra);
		itemCompra.setCompra(this);

		return itemCompra;
	}

	public ItemCompra removeItemCompra(ItemCompra itemCompra) {
		getItensCompra().remove(itemCompra);
		itemCompra.setCompra(null);

		return itemCompra;
	}
	
	public void popularCarrinho(HttpServletRequest request) {
		String[] ids_produto 	= request.getParameterValues("id_produto");
		String[] precos 		= request.getParameterValues("preco");
		String[] qtds_produto 	= request.getParameterValues("qtd_produto");
		//out.print(ids_produto[0]);
		int i = 0;
		for(String id_produto : ids_produto){
			Produto prdItemCompra = new Produto();
			prdItemCompra.setId(Integer.parseInt(id_produto));
			prdItemCompra = prdItemCompra.find();
			
			ItemCompra itemCarrinho = new ItemCompra();
			itemCarrinho.setProduto(prdItemCompra);
			itemCarrinho.setPreco(new BigDecimal(precos[i]));
			itemCarrinho.setQtd(Integer.parseInt(qtds_produto[i]));
			
			this.addItemCompra(itemCarrinho);
			
			i++;
		}
	}
	
	public float getTotalCompra() {
		float totalCompra = 0;
		
		for(ItemCompra itemCompra : this.getItensCompra()) {
			totalCompra += itemCompra.getPreco().floatValue() * itemCompra.getQtd();
		}
		
		return totalCompra;
	}
	

}