package model;

import java.io.Serializable;
import javax.persistence.*;

import database.DAO;

import java.math.BigDecimal;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * The persistent class for the item_compra database table.
 * 
 */
@Entity
@Table(name="item_compra")
@NamedQuery(name="ItemCompra.findAll", query="SELECT i FROM ItemCompra i")

@NamedQueries({
	@NamedQuery(name="ItemCompra.findAll", query="SELECT i FROM ItemCompra i"),
	@NamedQuery(name="ItemCompra.listByCompra", query="SELECT i FROM ItemCompra i WHERE i.compra = :compra")
})
public class ItemCompra extends DAO<ItemCompra> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private BigDecimal preco;

	//bi-directional many-to-one association to Compra
	@ManyToOne
	@JoinColumn(name="id_compra")
	private Compra compra;

	//bi-directional many-to-one association to Produto
	@ManyToOne
	@JoinColumn(name="id_produto")
	private Produto produto;

	private int qtd = 0;
	
	public ItemCompra() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getPreco() {
		return this.preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public Compra getCompra() {
		return this.compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public int getQtd() {
		return this.qtd;
	}

	public void setQtd(int qtd) {
		this.qtd = qtd;
	} 
	
	public JSONArray listByCompra(Compra compra){
		JSONArray jsonArrayItensCompra = null;
		try {
			List<ItemCompra> itensCompra = super.em.createNamedQuery("ItemCompra.listByCompra",ItemCompra.class)
					.setParameter("compra", compra)
					.getResultList();
			
			jsonArrayItensCompra = new JSONArray();
			for(ItemCompra itemCompra : itensCompra) {
				JSONObject jsonItemCompra = new JSONObject();
				jsonItemCompra.put("dirImage", itemCompra.getProduto().getDirImage());
				jsonItemCompra.put("nomeProduto", itemCompra.getProduto().getNome());
				jsonItemCompra.put("preco", itemCompra.getPreco());
				jsonItemCompra.put("qtd",itemCompra.getQtd());
				
				jsonArrayItensCompra.put(jsonItemCompra);
			}
			
			return jsonArrayItensCompra;
		}catch (Exception e) {
			return jsonArrayItensCompra;
			// TODO: handle exception
		}
	}

}