package model;

import java.io.File;
import java.io.Serializable;
import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import database.DAO;
import jdk.nashorn.internal.ir.RuntimeNode.Request;
import utils.Constants;
import utils.Util;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;


/**
 * The persistent class for the produto database table.
 * 
 */
@Entity
@Table(name="produto")

@NamedQuery(name="Produto.findAll", query="SELECT p FROM Produto p")
public class Produto extends DAO<Produto> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id = 0;

	@Column(name="codigo_barras")
	private String codigoBarras = "";

	@Column(name="dir_image")
	private String dirImage;

	private String nome = "";

	private BigDecimal preco = new BigDecimal(0);

	@Column(name="qtd_estoque")
	private int qtdEstoque = 0;

	private String status = "A";

	//bi-directional many-to-one association to ItemCompra
	@OneToMany(mappedBy="produto",cascade = CascadeType.ALL)
	private List<ItemCompra> itemCompras;

	public Produto() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigoBarras() {
		return this.codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public String getDirImage() {
		return this.dirImage;
	}

	public void setDirImage(String dirImage) {
		this.dirImage = dirImage;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getPreco() {
		return this.preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public int getQtdEstoque() {
		return this.qtdEstoque;
	}

	public void setQtdEstoque(int qtdEstoque) {
		this.qtdEstoque = qtdEstoque;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ItemCompra> getItemCompras() {
		return this.itemCompras;
	}

	public void setItemCompras(List<ItemCompra> itemCompras) {
		this.itemCompras = itemCompras;
	}

	public ItemCompra addItemCompra(ItemCompra itemCompra) {
		getItemCompras().add(itemCompra);
		itemCompra.setProduto(this);

		return itemCompra;
	}

	public ItemCompra removeItemCompra(ItemCompra itemCompra) {
		getItemCompras().remove(itemCompra);
		itemCompra.setProduto(null);

		return itemCompra;
	}
	
	public String getLabelProduto() {
		String labelProduto = this.getQtdEstoque() > 1 ? "produtos" : "produto";
		return this.getQtdEstoque() + " " + labelProduto;
	}
	
	public List<Produto> grid(){
		return super.em.createQuery("SELECT p FROM Produto p WHERE p.qtdEstoque > :qtdEstoque AND p.status = :status", Produto.class)
				.setParameter("qtdEstoque", 1)
				.setParameter("status", "A")
				.getResultList();
	}
	
	public boolean save(HttpServletRequest request) {
		String realPath = request.getServletContext().getRealPath(Constants.UPLOAD_DIRECTORY);
		
		
		if(ServletFileUpload.isMultipartContent(request)){
            try {
                
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(new ServletRequestContext(request));

                for(FileItem item : multiparts){
                    if(!item.isFormField()){
                    	System.out.println("É um arquivo...");
                    	try {
                    		String name = new File(item.getName()).getName();
                            
                            System.out.println(realPath + File.separator + name);
                            item.write(new File(realPath + File.separator + name));
                            this.setDirImage(".." + Constants.UPLOAD_DIRECTORY + File.separator + name);
                    	}catch(Exception e) {
                    		System.out.println("O arquivo deu problema: "+e.getMessage());
                    		e.getStackTrace();
                    	}
                    }else {
                    	mappingFields(item);
                    }
                }
                
               //File uploaded successfully
                
                this.save();
                return true;
                
            } catch (Exception ex) {
            	System.out.println("N Deu: "+ex.getMessage());
            	
            	return false;
            }          
          
        }else{
        	System.out.println("Deu muito ruim");
        	return false;
        	
        }
	}
	
	private void mappingFields(FileItem item) {
		switch(item.getFieldName()) {
			case "nome":
				this.setNome(item.getString());
				break;
			case "codigo_barras":
				this.setCodigoBarras(item.getString());
				break;
			case "preco":
				this.setPreco(new BigDecimal(item.getString()));
				break;
			case "qtd_estoque":
				this.setQtdEstoque(Integer.parseInt(item.getString()));
				break;
			case "id":
				this.setId(Integer.parseInt(item.getString()));
				break;
			case "last_dir_image":
				if(this.getDirImage() == null) {
					this.setDirImage(item.getString());
				}
				break;
		}
	}
	

	@Override
	public String toString() {
		return "Produto [id=" + id + ", codigoBarras=" + codigoBarras + ", dirImage=" + dirImage + ", nome=" + nome
				+ ", preco=" + preco + ", qtdEstoque=" + qtdEstoque + ", status=" + status + ", itemCompras="
				+ itemCompras + "]";
	}

}