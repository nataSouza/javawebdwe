package database;



import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import model.Produto;

import java.util.List;


public abstract class DAO<T>{
	@PersistenceContext
	protected EntityManager em;
	protected EntityManagerFactory emFactory;
	protected EntityTransaction eTransaction;
	public DAO(){
		this.emFactory = Persistence.createEntityManagerFactory("JavaWebDWE");
	    this.em = this.emFactory.createEntityManager();
	    this.em.getEntityManagerFactory().getCache().evictAll();
	    this.eTransaction = this.em.getTransaction();
	    this.eTransaction.begin();
	}
	
	public void save(){
		System.out.println(this.emFactory.getPersistenceUnitUtil().getIdentifier(this));
		try {
			if(Integer.parseInt(this.emFactory.getPersistenceUnitUtil().getIdentifier(this).toString()) > 0) {
				this.em.merge(this);
			}else {
				this.em.persist(this);
			}
			this.eTransaction.commit();
		}finally {
			this.em.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> list() {
		System.out.println(this.getClass().getSimpleName()+".findAll");
		return this.em.createNamedQuery(this.getClass().getSimpleName()+".findAll").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public T find(){
		return (T) this.em.find(this.getClass(), this.emFactory.getPersistenceUnitUtil().getIdentifier(this));
	}
	
	public boolean delete() {
		try {
	        this.em.remove(this.find());
	        this.eTransaction.commit();
			return true;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}finally {
			this.em.close();
		}
	}
}
