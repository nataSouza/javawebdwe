CREATE DATABASE poo CHARSET utf8;

use poo;

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL DEFAULT '',
  `pass` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `nivel` int(11) NOT NULL DEFAULT '1',
  `ativo` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`idUsuario`)
);
INSERT INTO `usuarios` VALUES 
	(1,'admin','admin','admin@localhost',3,'S'),					
	(4,'Juan','951','eeee@aaaa.com',11,'S'),
	(6,'root2','123456','a@b.c',99,'S'),
	(9,'Juan','951','eeee@aaaa.com',11,'S'),
	(10,'Juan','951','eeee@aaaa.com',11,'S');
